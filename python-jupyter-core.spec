%global _empty_manifest_terminate_build 0
Name:		python-jupyter-core
Version:	5.7.1
Release:	1
Summary:	Jupyter core package. A base package on which Jupyter projects rely.
License:	BSD-3-Clause
URL:		https://pypi.org/project/jupyter-core/
Source0:	%{url}/archive/%{version}/jupyter_core-%{version}.tar.gz
BuildArch:	noarch

Requires:	python3-traitlets

%description
Jupyter core package. A base package on which Jupyter projects rely.

%package -n python3-jupyter-core
Summary:	Jupyter core package. A base package on which Jupyter projects rely.
Provides:	python-jupyter-core = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-setuptools_scm
BuildRequires:	python3-pip
BuildRequires:	python3-hatchling
BuildRequires:	python3-wheel

%description -n python3-jupyter-core
Jupyter core package. A base package on which Jupyter projects rely.

%package help
Summary:	Development documents and examples for jupyter-core
Provides:	python3-jupyter-core-doc

%description help
Jupyter core package. A base package on which Jupyter projects rely.

%package -n python-jupyter-filesystem
Summary:        Jupyter filesystem layout

%description -n python-jupyter-filesystem
This package provides directories required by other packages that add
extensions to Jupyter.

%prep
%autosetup -n jupyter_core-%{version}

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/doclist.lst .
# Create directories for python-jupyter-filesystem package
mkdir -p %{buildroot}%{_datadir}/jupyter
mkdir %{buildroot}%{_datadir}/jupyter/kernels
mkdir -p %{buildroot}%{_datadir}/jupyter/labextensions/@jupyter
mkdir %{buildroot}%{_datadir}/jupyter/nbextensions
mkdir -p %{buildroot}%{_sysconfdir}/jupyter
mkdir %{buildroot}%{_sysconfdir}/jupyter/jupyter_notebook_config.d
mkdir %{buildroot}%{_sysconfdir}/jupyter/jupyter_server_config.d
mkdir %{buildroot}%{_sysconfdir}/jupyter/nbconfig
mkdir %{buildroot}%{_sysconfdir}/jupyter/nbconfig/common.d
mkdir %{buildroot}%{_sysconfdir}/jupyter/nbconfig/edit.d
mkdir %{buildroot}%{_sysconfdir}/jupyter/nbconfig/notebook.d
mkdir %{buildroot}%{_sysconfdir}/jupyter/nbconfig/terminal.d
mkdir %{buildroot}%{_sysconfdir}/jupyter/nbconfig/tree.d

%files -n python3-jupyter-core
%{_bindir}/jupyter*
%{python3_sitelib}/__pycache__/jupyter*
%{python3_sitelib}/jupyter.py
%{python3_sitelib}/jupyter_core-*.dist-info
%{python3_sitelib}/jupyter_core/

%files help -f doclist.lst
%{_docdir}/*

%files -n python-jupyter-filesystem
%{_datadir}/jupyter
%{_sysconfdir}/jupyter

%changelog
* Wed Mar 6 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 5.7.1-1
- Update package with version 5.7.1
  Derive JupyterAsyncApp from JupyterApp
  Modernize event loop behavior
  Enable async JupyterApp
  Clean up lint and add downstream checks
  Add python 3.12 support and update typings for traitlets 5.11

* Thu Aug 10 2023 li-miaomiao_zhr <mmlidc@isoftstone.com> - 5.3.1-2
- add python-jupyter-filesystem package

* Mon Jul 10 2023 li-miaomiao_zhr <mmlidc@isoftstone.com> - 5.3.1-1
- Update package to version 5.3.1

* Thu Nov 10 2022 wangjunqi <wangjunqi@kylinos.cn> - 5.0.0-1
- Update package to version 5.0.0

* Mon Jan 10 2022 Python_Bot <Python_Bot@openeuler.org> - 4.9.1-1
- Package Spec generated

